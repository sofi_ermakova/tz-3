import random

lines = []
for i in range(100):
    line = ""
    for el in range(100):
        line += str(random.randint(1, 10000))
        line += " "
    lines.append(line)
print(lines)

with open(r"file", "w") as file:
    file.writelines("%s\n" % line for line in lines)
file.close()

